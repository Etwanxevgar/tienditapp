//
//  FirebaseCore.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 10/03/21.
//

import Foundation
import FirebaseAnalytics
import FirebaseCrashlytics

class TienditAppCore {
    
    func analytics(typeR: String, data: [String?]) {
        Analytics.logEvent(AnalyticsEventSelectContent, parameters: [
            AnalyticsParameterItemID: data[0]!,
            AnalyticsParameterItemName: data[1]!,
            AnalyticsParameterContentType: data[2]!
          ])
    }
    
    func crashlyticsReport(screen: String, mensaje: String){
        let defaults = UserDefaults.standard
        if let email = defaults.value(forKey: "email") as? String {
            Crashlytics.crashlytics().setUserID(email) //Enviar el ID de usuario
            Crashlytics.crashlytics().setCustomValue(screen, forKey: "Screen report") //Enviar claves personalizadas
            Crashlytics.crashlytics().log(mensaje) //Envío de log de errores
        }
    }
    
}
