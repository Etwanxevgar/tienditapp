//
//  RegistroProdViewController.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 23/03/21.
//

import Foundation
import UIKit
import FirebaseAnalytics
import FirebaseFirestore
import FirebaseStorage

class RegistroProdViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var nombreProd: UITextField!
    @IBOutlet weak var barcodeTxt: UITextField!
    @IBOutlet weak var sizeProduct: UITextField!
    @IBOutlet weak var costProduct: UITextField!
    @IBOutlet weak var uploadPhoto: UIButton!
    @IBOutlet weak var previewButton: UIButton!
    @IBOutlet weak var scanButton: UIButton!
    @IBOutlet weak var saveButton: UIButton!
    
    
    
    private var imgData: Data?
    private var img: UIImage?
    private var urlImage: String?
    private var email: String?
    
    private let db = Firestore.firestore()
    private let storage = Storage.storage().reference()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Registro de Productos"
        Analytics.logEvent("screen_view", parameters: ["screen":self.title!])
        navigationItem.setHidesBackButton(true, animated: false)
        
        self.checkUD()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(RegistroProdViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
         
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    func obtenerCodigoBarras(codigo: String){
        if let barcodeTmp = codigo as String?{
            barcodeTxt.text = barcodeTmp
            self.buscarInfo(barcode: barcodeTmp)
        }
    }
    
    func buscarInfo(barcode: String){
        db.collection("stores").document(email!).collection("products").document(barcodeTxt.text!).getDocument{(documentSnapshot, error) in
            if let document = documentSnapshot, error == nil{
                if let cost = document.get("cost") as? String, let product = document.get("productName") as? String, let size = document.get("size") as? String, let urlImg = document.get("urlImage") as? String{
                    self.costProduct.text = cost
                    self.nombreProd.text = product
                    self.sizeProduct.text = size
                    self.uploadPhoto.isHidden = true
                    self.previewButton.isHidden = false
                    self.cargarImagen(urlImg: urlImg)
                }
            }
            
            self.nombreProd.isEnabled = true
            self.sizeProduct.isEnabled = true
            self.costProduct.isEnabled = true
            self.uploadPhoto.isEnabled = true
            self.saveButton.isEnabled = true
        }
    }
    
    func cargarImagen(urlImg: String){
        urlImage = urlImg
        let task = URLSession.shared.dataTask(with: URL(string: urlImg)!, completionHandler: {data, _, error in
            guard let data = data, error == nil else{
                return
            }
            
            DispatchQueue.main.async {
                self.productImg.image = UIImage(data: data)
            }
        })
        task.resume()
    }
    
    @IBAction func scannerButtonAction(_ sender: Any) {
        view.endEditing(true)
        let scvc = ScanBarcodeViewController()
        scvc.delegateVC = self
        self.present(scvc, animated: true, completion: nil)
    }
    
    @IBAction func uploadPhotoButtonAction(_ sender: Any) {
        view.endEditing(true)
        let picker = UIImagePickerController()
        picker.sourceType = .photoLibrary
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        view.endEditing(true)
        //Guardando foto producto en FireStorage
        DispatchQueue.main.async {
            self.storagePhoto(image: self.img!, uploadData: self.imgData!)
        }
    }
    @IBAction func previewButtonAction(_ sender: Any) {
        let vc = ProductPreviewViewController(productName: self.nombreProd.text!, size: self.sizeProduct.text!, cost: self.costProduct.text!, barcode: self.barcodeTxt.text!, urlImg: self.urlImage!)
        self.present(vc, animated: true, completion: nil)
    }
    
    func checkUD(){
        //Comprobar la sesión del usuario autenticado
        let defaults = UserDefaults.standard
        if let email = defaults.value(forKey: "email") as? String {
            self.email = email
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        view.endEditing(true)
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage else {
            return
        }
        
        self.img = image
        
        DispatchQueue.main.async {
            self.productImg.image = self.img
        }
        guard let imageData = image.jpegData(compressionQuality: 1.0) else{
            return
        }
        imgData = imageData
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func storagePhoto(image: UIImage, uploadData: Data){
        storage.child("products/\(self.barcodeTxt.text ?? "file").jpg").putData(uploadData, metadata: nil, completion: {_, error in
            guard error == nil else {
                print("Failed to upload")
                return
            }
            self.getURLImage()
        })
    }
    
    func getURLImage(){
        self.storage.child("products/\(self.barcodeTxt.text ?? "file").jpg").downloadURL(completion: {url, error in
            guard let url = url, error == nil else{
                return
            }
            self.urlImage = url.absoluteString
            print("Download URL: \(self.urlImage!)")
            if let barcode = self.barcodeTxt.text {
                //Registrar los productos de manera genérica/global
                /*db.collection("products").document(barcode).setData(["productName":nombreProd.text ?? "",
                                                                   "size": sizeProduct.text ?? "",
                                                                   "barcode": barcodeTxt.text ?? "",
                                                                   "cost": costProduct.text ?? "",
                                                                   "urlImage":urlImage ?? ""])*/
                
                //Para Registrar los datos de la tienda
                /*db.collection("stores").document(email!).setData(["Nombre":"EXI Store",
                                                                  "Direccion": "Av. Morelos #35, Depto Y302, Jamaica, 15800",
                                                                  "Correo": email!,
                                                                  "Telefono": "5560642384"])*/
                //Para registrar los productos en la tienda
                self.db.collection("stores").document(self.email!).collection("products").document(barcode).setData(      ["productName":self.nombreProd.text ?? "",
                     "size": self.sizeProduct.text ?? "",
                     "barcode": self.barcodeTxt.text ?? "",
                     "cost": self.costProduct.text ?? "",
                     "urlImage":self.urlImage ?? ""])
            
                let alert = UIAlertController(title: "", message: "Producto registrado", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
            
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now()){
                    alert.dismiss(animated: true, completion: { () in
                        self.nombreProd.text = ""
                        self.sizeProduct.text = ""
                        self.costProduct.text = ""
                        self.barcodeTxt.text = ""
                        self.productImg.image = nil
                    })
                }
            }
        })
    }
}
