//
//  UploadPhotoViewController.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 23/03/21.
//

import UIKit
import FirebaseStorage

class ProductPreviewViewController: UIViewController {
 
    @IBOutlet weak var productImg: UIImageView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var sizeProduct: UILabel!
    @IBOutlet weak var costProduct: UILabel!
    @IBOutlet weak var barcodeProduct: UILabel!
    @IBOutlet weak var addButton: UIButton!
    
    private var productNameS: String = ""
    private var sizeProductS: String = ""
    private var costProductS: String = ""
    private var barcodeS: String = ""
    private var urlImgS: String = ""
    
    init(productName: String, size: String, cost: String, barcode: String, urlImg: String){
        self.barcodeS = barcode
        self.productNameS = productName
        self.sizeProductS = size
        self.costProductS = cost
        self.urlImgS = urlImg
        super.init(nibName: "ProductPreviewViewController", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentView.layer.cornerRadius = 30
        cargarDatos()
    }
    
    func cargarDatos(){
        self.barcodeProduct.text = barcodeS
        self.sizeProduct.text = sizeProductS
        self.productName.text = productNameS
        self.costProduct.text = costProductS
        self.cargarImagen(urlImg: urlImgS)
    }
    func cargarImagen(urlImg: String){
        let task = URLSession.shared.dataTask(with: URL(string: urlImg)!, completionHandler: {data, _, error in
            guard let data = data, error == nil else{
                return
            }
            
            DispatchQueue.main.async {
                self.productImg.image = UIImage(data: data)
            }
        })
        task.resume()
    }
    
    @IBAction func addButtonAction(_ sender: Any) {
        
    }
    
}
