//
//  ScanBarcodeViewController.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 19/03/21.
//

import UIKit
import AVFoundation

class ScanBarcodeViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    private var barcode: String = ""
    
    var delegateVC = RegistroProdViewController()
    let scannerFrame = UIImageView()
    let barcodeValue = UILabel()
    
    var video = AVCaptureVideoPreviewLayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scannerFrameInit()
        barcodeLabelInit()
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection){
        
        if metadataObjects.count > 0 {
            if let object = metadataObjects[0] as? AVMetadataMachineReadableCodeObject{
                print(object.type)
                barcodeValue.text = object.stringValue
                let alert = UIAlertController(title: "", message: "Código escaneado", preferredStyle: .alert)
                self.present(alert, animated: true, completion: nil)
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1){
                    if let code = object.stringValue {
                        alert.dismiss(animated: true, completion: { () in self.delegateVC.obtenerCodigoBarras(codigo: code) })
                    }
                }
            }
        }
    }
    
    func scannerFrameInit() {
        scannerFrame.image = UIImage(named: "rectangleImage")
        scannerFrame.frame = CGRect(x: 0, y: 242, width: 374, height: 320)
        self.view.addSubview(scannerFrame)
        let session = AVCaptureSession()
        let captureDevice = AVCaptureDevice.default(for: AVMediaType.video)
        do{
            if captureDevice != nil {
                let input = try AVCaptureDeviceInput(device: captureDevice!)
                session.addInput(input)
            }
        }catch{
            print("ERROR")
        }
        let output = AVCaptureMetadataOutput()
        session.addOutput(output)
        output.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        output.metadataObjectTypes = [.qr, .ean13, .ean8, .upce, .code39, .code39Mod43, .code93, .code128, .pdf417, .aztec]
        video = AVCaptureVideoPreviewLayer(session: session)
        video.frame = view.layer.bounds
        view.layer.addSublayer(video)
        self.view.bringSubviewToFront(scannerFrame)
        session.startRunning()
    }

    func barcodeLabelInit() {
        barcodeValue.frame = CGRect(x: 0, y: 580, width: 374, height: 40)
        barcodeValue.text = "Código de Barras"
        barcodeValue.textAlignment = NSTextAlignment.center
        self.view.addSubview(barcodeValue)
        self.view.bringSubviewToFront(barcodeValue)
    }
}
