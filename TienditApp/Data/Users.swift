//
//  Users.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 23/03/21.
//

import Foundation

class Users {
    var name: String?
    var email: String?
    var address: String?
    var phone: String?
    var provider: String?
}
