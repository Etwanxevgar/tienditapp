//
//  Stores.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 23/03/21.
//

import Foundation

class Stores {
    var name: String?
    var address: String?
    var email: String?
    var phone: String?
    var branch: String?
}
