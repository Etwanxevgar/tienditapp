//
//  Products.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 23/03/21.
//

import Foundation

class Products {
    var barcode: String?
    var name: String?
    var cost: String?
    var size: String?
    var urlImage: String?
}
