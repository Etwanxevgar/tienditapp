//
//  Promotion.swift
//  TienditApp
//
//  Created by Oscar Sevilla Garduño on 23/03/21.
//

import Foundation

class Promotion {
    var barcode: String?
    var discount: String?
    var urlBanner: String?
}
